<?php

namespace Drupal\simple_tabs_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'TabsDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "TabsDefaultFormatter",
 *   label = @Translation("Tabs"),
 *   field_types = {
 *     "tabs"
 *   }
 * )
 */
class TabsDefaultFormatter extends FormatterBase {
    
    
     /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {}
 

  /**
   * Define how the field type is showed.
   * 
   * Inside this method we can customize how the field is displayed inside 
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'simple_tabs_field_formatter' , 
        '#type' => 'tabs',
        '#title' => $item->title,
        '#content' => $item->content,  
        
      ];
      //$elements['#theme']  = 'simple_tabs_field_formatter';
      
    }

    return $elements;
  }
  
}