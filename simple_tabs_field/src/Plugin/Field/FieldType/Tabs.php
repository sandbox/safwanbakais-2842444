<?php

namespace Drupal\simple_Tabs_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'address' field type.
 *
 * @FieldType(
 *   id = "tabs",
 *   label = @Translation("Simple Tabs"),
 *   description = @Translation("Creates a Bootstrap tab with content."),
 *   category = @Translation("General"),
 *   default_widget = "TabsDefaultWidget",
 *   default_formatter = "TabsDefaultFormatter"
 * )
 */
class Tabs extends FieldItemBase {

  /**
   * Field type properties definition.
   * 
   * Inside this method we defines all the fields (properties) that our 
   * custom field type will have.
   * 
   * Here there is a list of allowed property types: https://goo.gl/sIBBgO
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Title'))->setRequired(FALSE);

    $properties['content'] = DataDefinition::create('string')
      ->setLabel(t('Content'))->setRequired(FALSE);

    return $properties;
  }

  /**
   * Field type schema definition.
   * 
   * Inside this method we defines the database schema used to store data for 
   * our field type.
   * 
   * Here there is a list of allowed column types: https://goo.gl/YY3G7s
   */
  public static function schema(StorageDefinition $storage) {

    $columns = [];
    $columns['title'] = [
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,  
    ];
    $columns['content'] = [
      'type' => 'text',
      'size' => 'medium',
      'not null' => FALSE,    
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty. 
   * 
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field fype must be considered empty.
   */
  public function isEmpty() {

    $isEmpty = 
      empty($this->get('title')->getValue()) &&
      empty($this->get('content')->getValue());

    return $isEmpty;
  }

}