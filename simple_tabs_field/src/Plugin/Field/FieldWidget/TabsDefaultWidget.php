<?php

namespace Drupal\simple_tabs_field\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'TabsDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "TabsDefaultWidget",
 *   label = @Translation("Tabs create"),
 *   field_types = {
 *     "tabs"
 *   }
 * )
 */
class TabsDefaultWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   * 
   * Inside this method we can define the form used to edit the field type.
   * 
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  ) {

    // Title

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),

      // Set here the current value for this field, or a default value (or 
      // null) if there is no a value
      '#default_value' => isset($items[$delta]->title) ? 
          $items[$delta]->title : null,

      '#empty_value' => '',
      '#placeholder' => t('Title'),
    ];

    // Content

    $element['content'] = [
      '#type' => 'text_format',
      '#title' => t('Content'),
      '#format' => 'full_html',  
      '#default_value' => isset($items[$delta]->content) ? 
          $items[$delta]->content : null,
      '#empty_value' => '',
      //'#placeholder' => $this->getSetting(t('Content')),
    ];

    return $element;
  }
  
  
    /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

      foreach ($values as &$value) {
        if (count($value['content'])) {
          $value['content'] = $value['content']['value'];
        } else {
          $value['content'] = $value['content'] !== '' ? $value['content'] : '0';
        }
      }

    return $values;

  }



} 